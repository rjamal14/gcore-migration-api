namespace :customer_publish do
  desc 'schedule customer publish'
  task run: :environment do
    Migration::CustomerPublishService.new.execute
  end
end
