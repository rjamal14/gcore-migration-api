# Module response
module Response
  extend ActiveSupport::Concern

  def response_success(data, message)
    render json: { data: data, code: 200, status: :success, message: message }
  end

  def response_created(data, message)
    render json: { data: data, code: 201, created: true, message: message }
  end

  def response_updated(data, message)
    render json: { data: data, code: 200, success: true, message: message }
  end

  def response_error(data, message)
    render json: { status: data.errors, code: 400, success: true, message: message }
  end

  def response_bad(message)
    render json: { code: 400, message: message, success: false }
  end

  def response_ok(message)
    render json: { code: 200, success: true, message: message }
  end

  def response_not_found(message)
    render json: { code: 404, error: 'Not Found', message: message }
  end

  def response_index(datas, serial_datas)
    render json: { data: serial_datas,
                   code: 200,
                   status: 'success',
                   total_data: datas.total_count,
                   total_page: datas.total_pages,
                   current_page: datas.current_page,
                   prev_page: datas.prev_page,
                   next_page: datas.next_page }, status: 200
  end

  def response_index_transactions(datas, serial_datas, datas2)
    render json: { data: serial_datas,
                   transaction: datas2,
                   code: 200,
                   status: 'success',
                   total_data: datas.total_count,
                   total_page: datas.total_pages,
                   current_page: datas.current_page,
                   prev_page: datas.prev_page,
                   next_page: datas.next_page }, status: 200
  end

  def response_auth_failed
    render json: { code: '403', status: 'forbidden', message: 'invalid access token' }, status: 403
    nil
  end

  def response_auth_timeout
    render json: { code: '400', status: 'timeout', message: 'connection timeout' }, status: 400
    nil
  end
end
