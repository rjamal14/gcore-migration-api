# APP COntroller
class ApplicationController < ActionController::API
  include Response
  before_action :current_user

  def auth
    @auth ||= Auth::AuthorizeService.new
  end

  def current_user
    render json: { message: 'Unauthorize' }, status: :unauthorized unless request.headers['Authorization'].present?

    auth.set_token(request.headers['Authorization'])
    @current_user ||= auth.current_user

    response_auth_failed if @current_user.nil?
    @current_user
  end

  def get_master(url)
    auth.get_master(url)
  end

  def generate_number(type)
    get_master("number_formats/generate?type=#{type}")['data']
  end
end
