module Api
  module V1
    class ImportsController < ApplicationController
      def index
        @imports = if params[:search]
                     Import.where(name: /.*#{params[:search]}.*/i)
                   else
                     Import
                    end
        @imports = @imports.page(params[:page] || 1).per(params[:per_page] || 10)
                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")

        response_index(@imports, @imports)
      end

      def create
        # data from api form-data
        file = params[:file]

        @import = Import.new(
          file_name: file.original_filename,
          file_type: file.content_type,
          file_url: file,
          type: params[:type],
          status: 'pending'
        )

        response_created(@import, 'Data Sedang di proses')

        @import.save

      end
    end
  end
end
