module Api
  module V1
    # Class Regional
    class RegionalsController < ApplicationController
      # POST /regionals
      def create
        @regional = Regional.new(regional_params.merge(created_by_id: current_user))

        if @regional.save
          response_created(@regional, 'Data regional berhasil tersimpan')
        else
          response_error(@regional, 'Data regional gagal tersimpan')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def regional_params
        params.require(:regional).permit(:name, :code)
      end
    end
  end
end
