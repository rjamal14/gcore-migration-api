# area Controller
module Api
  module V1
    # area Controller
    class AreasController < ApplicationController
      # POST /areas
      def create
        @area = Area.new(area_params.merge(created_by_id: current_user))

        if @area.save
          response_created(@area, 'Data Kota Tersimpan')
        else
          response_error(@area, 'Data Kota Gagal Tersimpan')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def area_params
        params.require(:area).permit(:name, :code, :regional_id)
      end
    end
  end
end
