module Api
  module V1
    # Class Kantor Unir
    class UnitOfficesController < ApplicationController
      # POST /unit_offices
      def create
        @unit_office = UnitOffice.new(unit_office_params.merge(created_by_id: current_user))

        if @unit_office.save
          response_created(@unit_office, 'Data Kantor Unit tersimpan')
        else
          response_error(@unit_office, 'Data Kantor Unit gagal tersimpan')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def unit_office_params
        params.require(:unit_office).permit(:name, :code, :city_id, :branch_office_id, :description, :address, :latitude, :longitude)
      end
    end
  end
end
