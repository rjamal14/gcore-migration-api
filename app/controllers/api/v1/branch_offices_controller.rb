# Controller Kantor Cabang
module Api
  module V1
    # Controller Kantor Cabang
    class BranchOfficesController < ApplicationController
      # POST /branch_offices
      def create
        @branch_office = BranchOffice.new(branch_office_params.merge(created_by_id: current_user))

        if @branch_office.save
          response_created(@branch_office, 'Data kantor cabang tersimpan')
        else
          response_error(@branch_office, 'Data kantor cabang gagal tersimpan')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def branch_office_params
        params.require(:branch_office).permit(:name, :code, :city_id, :area_id, :address, :latitude, :longitude, :description)
      end
    end
  end
end
