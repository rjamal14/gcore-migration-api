module Api
  module V1
    class CustomersController < ApplicationController
      # POST /customer

      def create
        @customer = Customer.new(customer_params.merge(created_by_id: current_user.id))

        if @customer.save
          response_created(@customer, 'Data Tersimpan')
        else
          response_error(@customer, 'Data Gagal Tersimpan')
        end
      end

      private

      # Only allow a trusted parameter "white list" through.
      def customer_params
        params.require(:customer).permit(:cif_number, :branch_office_id, :name,
                                         :identity_number, :identity_type, :expired_date, :birth_date, :degree,
                                         :id_scan_image, :mother_name, :birth_place, :gender, :status, :marital_status,
                                         customer_contact_attributes: [:id, :_destroy, :residence_address, :residence_province, :residence_city,
                                                                       :residence_region, :residence_postal_code, :identity_address,
                                                                       :identity_province, :identity_city, :identity_region,
                                                                       :identity_postal_code, :phone_number, :telephone_number],
                                         customer_job_attributes: [:id, :_destroy, :job_type, :company_name, :tax_number, :income_source,
                                                                   :disbursement_type, :customer_type, :financing_purpose,
                                                                   :salary_amount],
                                         customer_notes_attributes: [:id, :_destroy, :subject, :note_description],
                                         customer_emergency_contacts_attributes: [:id, :_destroy, :name, :relation, :nationality, :job_name, :address],
                                         customer_bank_accounts_attributes: [:id, :_destroy, :account_name, :account_number, :main_account, :bank_name, :branch_name])
      end
    end
  end
end
