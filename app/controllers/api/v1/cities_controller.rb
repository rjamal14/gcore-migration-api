# City Controller
module Api
  module V1
    # City Controller
    class CitiesController < ApplicationController
      # POST /cities
      def create
        @city = City.new(city_params.merge(created_by_id: current_user))

        if @city.save
          response_created(@city, 'Data Kota Tersimpan')
        else
          response_error(@city, 'Data Kota Gagal Tersimpan')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def city_params
        params.require(:city).permit(:name, :province_id)
      end
    end
  end
end
