module Api
  # Module V1
  module V1
    # Class transaction_imports
    class TransactionImportsController < ApplicationController
      # POST /transaction_imports
      def index
        @transaction_imports = TransactionImport.all
        response_success(@transaction_imports, 'List all data')
      end
    end
  end
end
