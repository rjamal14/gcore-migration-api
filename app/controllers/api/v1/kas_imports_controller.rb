module Api
  # Module V1
  module V1
    # Class kas_imports
    class KasImportsController < ApplicationController
      # POST /kas_imports
      def index
        @kas_imports = KasImport.all
        response_success(@kas_imports, 'List all data')
      end
    end
  end
end
