module Api
  # Module V1
  module V1
    # Class customer_imports
    class CustomerImportsController < ApplicationController
      # POST /customer_imports
      def index
        @customer_imports = CustomerImport.all
        response_success(@customer_imports, 'List all data')
      end
    end
  end
end
