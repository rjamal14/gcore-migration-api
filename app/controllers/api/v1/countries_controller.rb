module Api
  # Module V1
  module V1
    # Class Countrues
    class CountriesController < ApplicationController
      # POST /countries
      def create
        @country = Country.new(country_params.merge(created_by_id: current_user))
        if @country.save
          response_created(@country, 'Data Negara Tersimpan')
        else
          response_error(@country, 'Input Data Negara Gagal')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def country_params
        params.require(:country).permit(:name, :nationality)
      end
    end
  end
end
