module Api
  module V1
    # Province Controller
    class ProvincesController < ApplicationController
      # POST /provinces
      def create
        @province = Province.new(province_params.merge(created_by_id: current_user))

        if @province.save
          response_created(@province, 'Data provinsi tersimpan')
        else
          response_error(@province, 'Data provinsi gagal tersimpan')
        end
      end

      # Only allow a trusted parameter "white list" through.
      def province_params
        params.require(:province).permit(:name, :country_id)
      end
    end
  end
end
