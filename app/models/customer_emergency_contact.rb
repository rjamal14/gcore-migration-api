# CUstomer Emergency Contact
class CustomerEmergencyContact
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :prior_customer_id, presence: true
  validates :name, presence: true
  validates :relation, presence: true
  validates :address, presence: true

  field :prior_customer_id, type: BSON::ObjectId
  field :name, type: String
  field :relation, type: String
  field :nationality, type: String
  field :address, type: String
  field :job_name, type: String
  field :deleted_at, type: DateTime

  belongs_to :customer, foreign_key: :prior_customer_id, optional: true
end
