class CustomerGhanet
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  field :prior_customer_id, type: String
end
