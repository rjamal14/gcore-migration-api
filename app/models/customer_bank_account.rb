# Bank Model
class CustomerBankAccount
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :prior_customer_id, presence: true

  field :prior_customer_id, type: BSON::ObjectId
  field :account_number, type: String
  field :account_name, type: String
  field :bank_name, type: String
  field :branch_name, type: String
  field :deleted_at, type: DateTime
  field :main_account, type: Boolean, default: false

  belongs_to :customer, foreign_key: :prior_customer_id, optional: true
end
