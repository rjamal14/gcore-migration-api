# model Transaction
class TransactionImport
  include Mongoid::Document

  field :nosbk, type: Integer
  field :nic, type: Integer
  field :nama, type: String
  field :tgsbk, type: String
  field :tempo, type: String
  field :lelang, type: String
  field :taksir, type: Integer
  field :up, type: Integer
  field :admin, type: Integer
  field :keter1, type: String
  field :keter2, type: String
  field :keter3, type: String
  field :noktp, type: Float
  field :alamat, type: String
  field :alamat2, type: String
  field :telpun, type: Integer
  field :jnsbrg, type: String
  field :golbrg, type: String
  field :keter4, type: String
  field :sewamodal, type: Float
  field :jangka, type: Integer
  field :cicil, type: Integer
  field :status, type: String
  field :jenisbmh, type: String
end
