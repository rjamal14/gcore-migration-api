# CustomerContact
class CustomerContact
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :prior_customer_id, presence: true

  field :prior_customer_id, type: BSON::ObjectId
  field :residence_address, type: String
  field :residence_province, type: String
  field :residence_city, type: String
  field :residence_region, type: String
  field :residence_postal_code, type: String
  field :identity_address, type: String
  field :identity_province, type: String
  field :identity_city, type: String
  field :identity_region, type: String
  field :identity_postal_code, type: String
  field :phone_number, type: String
  field :telephone_number, type: String
  field :deleted_at, type: DateTime

  belongs_to :customer, foreign_key: :prior_customer_id, optional: true
end
