class UnitMap
  include Mongoid::Document
  
  field :id_lama, type: Integer
  field :id_baru, type: String
  field :name, type: String
end