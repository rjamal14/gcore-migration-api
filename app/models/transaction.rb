class Transaction
  extend Enumerize
  include Mongoid::Document
  include Mongoid::Timestamps

  enumerize :status, in: [:draft, :active, :repayment, :canceled, :completed]
  enumerize :transaction_type, in: [:funding, :repayment, :prolongation, :auction, :cancelled]
  enumerize :disbursement_status, in: [:pending, :on_progress, :complete], default: :complete

  field :prior_transaction_id, type: Integer
  field :insurance_item_id, type: String
  field :customer_id, type: Integer
  field :company_id, type: Integer
  field :product_id, type: String
  field :status
  field :transaction_type
  field :disbursement_status
  field :sge, type: String
  field :contract_date, type: Date
  field :due_date, type: Date
  field :auction_date, type: Date
  field :loan_amount, type: Integer
  field :admin_fee, type: Integer
  field :monthly_fee, type: Integer
  field :maximum_loan, type: Float
  field :maximum_loan_percentage, type: Float
  field :stle_rate, type: Float
  field :notes, type: String
  field :cif_number, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :deleted_by_id, type: String

  has_many :transaction_insurance_items, dependent: :destroy, foreign_key: :prior_transaction_id
end
