class Import
  extend Enumerize
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  validates :file_name, presence: true
  validates :file_type, presence: true
  validates :status, presence: true
  validates :type, presence: true

  field :file_url, type: String
  field :file_name, type: String
  field :file_type, type: String
  field :row_total, type: Integer
  field :row_success, type: Integer
  field :row_failed, type: Integer
  field :status
  field :type

  enumerize :status, in: [:pending, :onprogress, :success]
  enumerize :type, in: [:customer, :transaction]

  mount_uploader :file_url, ImportUploader

  has_many :import_row_errors, dependent: :destroy

  accepts_nested_attributes_for :import_row_errors, allow_destroy: true

  after_create do
    Migration::ImportService.new(self).execute
  end
end
