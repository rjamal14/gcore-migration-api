class ImportRowError
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  validates :row_request, presence: true
  validates :row_message_error, presence: true

  field :import_id, type: BSON::ObjectId
  field :row_request, type: Hash
  field :row_message_error, type: String

  belongs_to :import, foreign_key: :import_id, optional: true
end
