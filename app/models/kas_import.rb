# model Transaction
class KasImport
  include Mongoid::Document

  field :perk, type: Integer
  field :jumlah, type: Integer
  field :uraian, type: String
  field :trans, type: String
  field :tanggal, type: String
  field :kdkas, type: String
end
