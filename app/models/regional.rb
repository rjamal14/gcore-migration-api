# Model Regional
class Regional
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :code, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String

  validates_presence_of :name, :code
  validates_uniqueness_of :code, :name

  has_many :areas, foreign_key: 'regional_id'
end
