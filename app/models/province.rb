# Model Province
class Province
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :country_id, type: String

  validates_presence_of :name, :country_id
  validates_uniqueness_of :name

  has_many :cities, foreign_key: 'province_id'
  belongs_to :country, foreign_key: 'country_id', inverse_of: :provinces, optional: true
end
