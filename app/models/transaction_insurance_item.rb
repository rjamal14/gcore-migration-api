# Transaction Insurance Item Model
class TransactionInsuranceItem
  include Mongoid::Document
  include Mongoid::Timestamps

  field :prior_transaction_id, type: Integer
  field :product_insurance_item_id, type: String
  field :name, type: String
  field :quantity, type: Integer
  field :ownership, type: String
  field :carats, type: Float
  field :weight, type: Float
  field :description, type: String
  field :foreacast, type: Float
  field :maximum_loan, type: Float

  belongs_to :item_transaction, class_name: 'Transaction', foreign_key: :prior_transaction_id, optional: true
end
