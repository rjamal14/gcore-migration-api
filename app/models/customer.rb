# model customers
class Customer
  extend Enumerize
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Rails.application.routes.url_helpers
  include ActiveModel::Validations

  validates :prior_customer_id, presence: true
  validates :cif_number, presence: true
  validates :name, presence: true
  validates :gender, presence: true
  validates :identity_number, presence: true
  validates :identity_type, presence: true
  validates :birth_date, presence: true
  validates :birth_place, presence: true
  validates :mother_name, presence: true
  validates :marital_status, presence: true
  validates :status, presence: true
  validates_uniqueness_of :cif_number, :identity_number, :prior_customer_id

  field :prior_customer_id, type: BSON::ObjectId
  field :cif_number, type: String
  field :branch_office_id, type: String
  field :branch_office_name, type: String
  field :name, type: String
  field :degree, type: String
  field :gender
  field :identity_number, type: String
  field :identity_type
  field :expired_date, type: Date
  field :birth_date, type: Date
  field :birth_place, type: String
  field :id_scan_image
  field :mother_name, type: String
  field :marital_status
  field :status, type: Boolean, default: false
  field :created_by_id, type: BSON::ObjectId
  field :updated_by_id, type: BSON::ObjectId
  field :deleted_by_id, type: BSON::ObjectId
  field :deleted_at, type: DateTime

  enumerize :marital_status, in: [:single, :married, :divorced]
  enumerize :gender, in: [:l, :p]
  enumerize :identity_type, in: [:ktp, :sim, :passport]

  mount_base64_uploader :id_scan_image, CustomerUploader, file_name: ->(u) { u.name }

  has_many :customer_notes, dependent: :destroy, foreign_key: :prior_customer_id
  has_one :customer_contact, dependent: :destroy, foreign_key: :prior_customer_id
  has_one :customer_job, dependent: :destroy, foreign_key: :prior_customer_id
  has_many :customer_emergency_contacts, dependent: :destroy, foreign_key: :prior_customer_id
  has_many :customer_bank_accounts, dependent: :destroy, foreign_key: :prior_customer_id

  accepts_nested_attributes_for :customer_emergency_contacts, allow_destroy: true
  accepts_nested_attributes_for :customer_job, allow_destroy: true
  accepts_nested_attributes_for :customer_contact, allow_destroy: true
  accepts_nested_attributes_for :customer_notes, allow_destroy: true
  accepts_nested_attributes_for :customer_bank_accounts, allow_destroy: true
end
