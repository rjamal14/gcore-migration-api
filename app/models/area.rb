# Model
class Area
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :code, type: String
  field :regional_id, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String

  validates_presence_of :name, :code, :regional_id
  validates_uniqueness_of :name, :code

  has_many :office_hour_schedules
  has_many :branch_offices, foreign_key: 'city_id'
  belongs_to :regional, inverse_of: :areas, foreign_key: 'regional_id', optional: true
end
