# City Model
class City
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :province_id, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String

  validates_presence_of :name, :province_id
  validates_uniqueness_of :name

  has_many :unit_offices, foreign_key: 'city_id', primary_key: 'id'
  has_many :branch_offices, foreign_key: 'city_id', primary_key: 'id'
  belongs_to :province, foreign_key: 'province_id', inverse_of: :cities, optional: true
  belongs_to :area, foreign_key: 'area_id', inverse_of: :cities, optional: true
end
