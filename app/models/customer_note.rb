# CustomerNote
class CustomerNote
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :customer_id, presence: true
  validates :subject, presence: true
  validates :note_description, presence: true

  field :customer_id, type: BSON::ObjectId
  field :subject, type: String
  field :note_description, type: String
  field :deleted_at, type: DateTime

  belongs_to :customer, foreign_key: :customer_id
end
