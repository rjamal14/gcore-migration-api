# Model Kantor Unit
class UnitOffice
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :code, type: String
  field :city_id, type: String
  field :branch_office_id, type: String
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String
  field :address, type: String
  field :latitude, type: BigDecimal
  field :longitude, type: BigDecimal

  validates_uniqueness_of :name, scope: :city_id
  validates_presence_of :name, :code, :city_id, :branch_office_id

  belongs_to :city, foreign_key: 'city_id', primary_key: 'id', optional: true
  belongs_to :branch_office, foreign_key: 'branch_office_id', primary_key: 'id', optional: true
end
