class Country
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :nationality, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String

  validates_presence_of :name, :nationality
  validates_uniqueness_of :name

  has_many :provinces, foreign_key: 'country_id'
end
