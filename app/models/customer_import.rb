# model customers
class CustomerImport
  include Mongoid::Document

  field :nomor, type: Integer
  field :nama, type: String
  field :telpun, type: Integer
  field :tgktp, type: String
  field :tglahir, type: String
  field :tmlahir, type: String
  field :alamat, type: String
  field :alamat2, type: String
  field :no_ktp
  field :stehir, type: String
  field :slahir, type: String
  field :rt, type: Integer
  field :rw, type: Integer
  field :saudara, type: String
  field :alamats, type: String
  field :alamats2, type: String
  field :status, type: String
  field :camat, type: String
  field :kota, type: String
  field :provinsi, type: String
  field :kerja, type: String
  field :ibu, type: String
  field :warga, type: String
  field :lurah, type: String
  field :kodepos, type: Integer
  field :sex, type: String
  field :kerjas, type: String
  field :hubung, type: String
end
