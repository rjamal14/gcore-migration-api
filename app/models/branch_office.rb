# Model Kantor Cabang
class BranchOffice
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :name, type: String
  field :code, type: String
  field :city_id, type: String
  field :area_id, type: String
  field :address, type: String
  field :latitude, type: BigDecimal
  field :longitude, type: BigDecimal
  field :description, type: String
  field :created_by_id, type: String
  field :updated_by_id, type: String

  has_many :unit_offices, foreign_key: 'branch_office_id', primary_key: 'id'
  belongs_to :city, foreign_key: 'city_id', inverse_of: :branch_offices, optional: true
  belongs_to :area, foreign_key: 'area_id', inverse_of: :branch_offices, optional: true

  validates_presence_of :name, :code, :city_id
  validates_uniqueness_of :name, :code, :area_id
end
