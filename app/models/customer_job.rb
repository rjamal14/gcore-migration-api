# CUstomer Job Model
class CustomerJob
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include ActiveModel::Validations

  validates :prior_customer_id, presence: true

  field :prior_customer_id, type: BSON::ObjectId
  field :job_type, type: String
  field :company_name, type: String
  field :customer_type, type: String
  field :tax_number, type: String
  field :income_source, type: String
  field :disbursement_type, type: String
  field :financing_purpose, type: String
  field :salary_amount, type: Integer
  field :deleted_at, type: DateTime

  belongs_to :customer, foreign_key: :prior_customer_id, optional: true
end
