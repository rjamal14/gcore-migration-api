require 'csv'

module Migration
  # migration customer service
  class CustomerService
    def initialize(id, data)
      @import = {
        id: id,
        data: data
      }
      @datas_success = []
      @datas_error = []
      @import_table = Import.find(@import[:id])
    end

    def auth
      @auth ||= Auth::AuthorizeService.new
    end

    def generate_number(type, office_id, office_type)
      auth.get_office("number_formats/generate?type=#{type}&office_id=#{office_id}&office_type=#{office_type}")['data']
    end

    def formating_customer(customer)
      branch_office = UnitMap.find_by(id_lama: customer.id_unit) if customer.id_unit.present?
      tmp_data_format = {
        prior_customer_id: customer.id,
        cif_number: generate_number('cif', branch_office.try(:id_baru), 'unit'),
        branch_office_id: branch_office.try(:id_baru),
        branch_office_name: branch_office.try(:name),
        name: customer.name,
        gender: customer.gender == 'FEMALE' ? 'p' : 'l',
        identity_number: customer.nik,
        identity_type: 'ktp',
        birth_date: customer.birth_date,
        birth_place: customer.birth_place,
        mother_name: customer.mother_name,
        marital_status: (customer.marital == 'UNDEFINED') ? 'single' : 'married',
        status: false,
        customer_contact_attributes: customer_contact(customer),
        customer_emergency_contacts_attributes: customer_emergency_contact(customer)
      }

      create_customer(tmp_data_format)
    end

    def customer_contact(customer)
      address = get_address({
                              subdistric: customer.kelurahan,
                              city: customer.city,
                              region: customer.kecamatan,
                              province: customer.province
                            })

      data_customer_contact = {
        residence_address: "#{customer.address} RT/RW #{customer.rt}/#{customer.rw}",
        residence_province: address['province'],
        residence_city: address['city'],
        residence_region: address['region'],
        residence_postal_code: customer.kodepos,
        identity_address: '',
        identity_province: address['province'],
        identity_city: address['city'],
        identity_region: address['region'],
        identity_postal_code: customer.kodepos,
        phone_number: customer.mobile,
        telephone_number: ''
      }

      data_customer_contact
    end

    def customer_emergency_contact(customer)
      data_customer_emergency_contact = [{
        name: customer.sibling_name,
        relation: customer.sibling_relation,
        nationality: 'WNI',
        address: "#{customer.sibling_address_1} #{customer.sibling_address_2}",
        job_name: customer.sibling_job
      }]

      data_customer_emergency_contact
    end

    def get_address(payload)
      auth.set_token
      data_address = auth.post_master('address/find', payload)
      data_address['data']
    end

    def create_customer(customer)
      customer_new = Customer.new(customer)
      if customer_new.save
        @datas_success << customer_new
      else
        @datas_error << customer_new
        ImportRowError.create(import_id: @import[:id],
                              row_request: customer,
                              row_message_error: customer_new.errors.full_messages)
      end
    end

    def execute
      @import[:data].each do |customer|
        formating_customer(customer)
      end

      @import_table.update(row_success: @datas_success.length,
                           row_failed: @datas_error.length,
                           status: 'success')
    end
  end
end
