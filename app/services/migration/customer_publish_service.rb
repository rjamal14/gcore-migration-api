# Journal
require 'json'
require 'base64'

module Migration
  class CustomerPublishService
    def initialize
      @queue_name = "#{ENV['rbmq_env']}-migrations.customers.create"
      @customers = Customer.all
    end

    def execute
      publish
    end

    def channel
      @channel ||= ::Publisher::BunnyPublisher.connection.create_channel
    end

    def exchange
      @exchange ||= channel.exchange(
        "#{ENV['rbmq_env']}-migrations",
        type: 'direct',
        durable: true
      )
    end

    def publish
      @customers.each do |customer|
        customer_attribute = customer.attributes
        customer_attribute['customer_contact_attributes'] = customer.customer_contact.attributes
        customer_attribute['customer_contact_attributes'].delete('_id')
        customer_attribute['customer_contact_attributes'].delete('prior_customer_id')
        customer_attribute['customer_emergency_contacts_attributes'] = []

        customer.customer_emergency_contacts.each do |customer_emergency_contact|
          customer_emergency = {
            name: customer_emergency_contact.name,
            relation: customer_emergency_contact.relation,
            nationality: customer_emergency_contact.nationality,
            address: customer_emergency_contact.address,
            job_name: customer_emergency_contact.job_name
          }

          customer_attribute['customer_emergency_contacts_attributes'] << customer_emergency
        end

        exchange.publish(customer_attribute.to_json, routing_key: @queue_name)
      end

      channel.close
    end
  end
end
