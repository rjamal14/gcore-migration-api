require 'csv'

module Migration
  class ImportService
    def initialize(data)
      @data_import = data
      @row_header = %w[]
      @datas_row = []
      @datas_format = []
    end

    def execute
      @data_import.update(status: 'onprogress')
      @datas_row = CSV.read("public#{@data_import.file_url.url}")
      formating_customer if @data_import.type == 'customer'
    end

    def formating_customer
      @datas_row.each_with_index do |value, key|
        tmp_data = {}
        value.each_with_index do |row, index|
          if key == 0
            @row_header << row
          else
            tmp_data[@row_header[index]] = row if index > 0
            tmp_data['prior_customer_id'] = row if index == 0
          end
        end

        next unless key > 0

        customer = CustomerGhanet.new(tmp_data)
        @datas_format << customer if customer.save
      end

      update_count_data
    end

    def update_count_data
      @data_import.update(row_total: @datas_format.length,
                          row_success: 0,
                          row_failed: 0)

      Migration::CustomerService.new(@data_import.id, @datas_format).execute
    end
  end
end
