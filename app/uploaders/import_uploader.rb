# Customer Uploader
class ImportUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{model.type}/#{model.id}"
  end

  def extension_whitelist
    %w[csv txt xls]
  end
end
