class TransactionCreateWorker
  include Sneakers::Worker

  queue_name = 'transactions.create'
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data_transaction = {
      'prior_transaction_id' => data['id'],
      'insurance_item_id' => data['collateral_category_id'],
      'customer_id' => data['customer_id'],
      'product_id' => data['product_id'],
      'status' => data['status'],
      'transaction_type' => '-',
      'disbursement_status' => '-',
      'sge' => data['sbg_number'],
      'contract_date' => data['generate_contract_at'],
      'due_date' => data['due_date'],
      'auction_date' => data['auction_date'],
      'loan_amount' => data['loan_by_estimator'],
      'admin_fee' => data['admin_fee'],
      'monthly_fee' => (data['loan_by_estimator'] * data['deposit_rate'] / 100) * 2,
      'maximum_loan' => data['admin_fee'],
      'maximum_loan_percentage' => data['maximum_loan_percentage'],
      'stle_rate' => data['stle_rate'],
      'notes' => data['notes'],
      'cif_number' => data['cif_number']
    }
    Transaction.new(data_transaction.to_h).save!
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateWorker => #{e.message}"
    reject!
  end
end
