class CustomerCreateJobExperiencesWorker
  include Sneakers::Worker

  queue_name = 'customers.create.job_experiences'
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data_customer = {
      'prior_customer_id' => data['prior_customer_id'],
      'job_type' => 'other',
      'company_name' => data['job'][0] ? data['job'][0]['company_name'] : '-',
      'customer_type' => 'other',
      'tax_number' => data['npwp'],
      'income_source' => 'other',
      'disbursement_type' => 'other',
      'financing_purpose' => 'other',
      'salary_amount' => 5_000_000
    }
    CustomerJob.new(data_customer.to_h).save!
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateJobExperiencesWorker => #{e.message}"
    reject!
  end
end
