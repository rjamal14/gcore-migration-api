class CustomerCreateBankAccountsWorker
  include Sneakers::Worker

  queue_name = 'customers.create.bank_accounts'
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data.each do |value|
      CustomerBankAccount.new(value.to_h).save!
    end
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateBankAccountsWorker => #{e.message}"
    reject!
  end
end
