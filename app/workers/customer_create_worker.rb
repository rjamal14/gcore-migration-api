class CustomerCreateWorker
  include Sneakers::Worker

  queue_name = 'customers.create'
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data_customer = {
      'prior_customer_id' => data['prior_customer_id'],
      'cif_number' => data['cif_number'],
      'branch_office_id' => data['branch_office_id'],
      'name' => data['name'],
      'gender' => data['gender'] == 'man' ? 'l' : 'p',
      'identity_number' => data['identity_number'],
      'identity_type' => 'ktp',
      'birth_date' => data['birth_date'],
      'birth_place' => data['birth_place'],
      'marital_status' => !(data['marital_status'] == 'single'),
      'degree' => 'other',
      'mother_name' => data['mother_name'],
      'id_scan_image' => data['id_scan_image']
    }
    Customer.new(data_customer.to_h).save!
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateWorker => #{e.message}"
    reject!
  end
end
