class TransactionCreateItemWorker
  include Sneakers::Worker

  queue_name = 'transactions.create.item'
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data.each do |value|
      TransactionInsuranceItem.new(value.to_h).save!
    end
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateWorker => #{e.message}"
    reject!
  end
end
