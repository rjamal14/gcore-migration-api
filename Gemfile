source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.4'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3', '>= 6.0.3.2'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# mongid for mongodb
gem 'mongoid', '~> 7.1.2'

# enum
gem 'enumerize', '~> 2.3.1'

# roo for read xls or csv
gem 'roo', '~> 2.8.0'

gem 'bunny', '>= 2.13.0', require: true
gem 'carrierwave-base64', '~> 2.8.1'
gem 'carrierwave-mongoid', '~> 1.3.0'
gem 'dotenv-rails', '~> 2.7.6', require: 'dotenv/rails-now'
gem 'kaminari-mongoid'
gem 'mongoid_paranoia', '~> 0.4.0'
gem 'oauth2', '~> 1.4.4'
gem 'rack-cors', '~> 1.1.1'
gem 'rails-i18n', '~> 6.0.0'
gem 'rest-client', '~> 2.1.0'
gem 'rubocop', '~> 0.88.0'
gem 'sneakers', '>=2.11.0'
gem 'whenever', '~> 1.0.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'capistrano', '~> 3.10'
  gem 'capistrano3-puma', '~> 4.0'
  gem 'capistrano-bundler', '~> 1.4'
  gem 'capistrano-rails', '~> 1.4', require: false
  gem 'capistrano-rbenv', '~> 2.2'
  gem 'listen', '~> 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
