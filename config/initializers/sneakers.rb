require 'sneakers'
require 'sneakers/handlers/maxretry'

Sneakers.configure connection: Bunny.new(
  host: ENV['rbmq_host'],
  vhost: ENV['rbmq_vhost'],
  user: ENV['rbmq_user'],
  password: ENV['rbmq_password'],
  logger: Rails.logger
),
                   exchange: "#{ENV['rbmq_env']}-migrations-core",
                   exchange_type: :direct,
                   runner_config_file: nil,
                   metric: nil,
                   workers: 2,
                   log: './log/sneakers.log',
                   pid_path: './tmp/pids/sneakers.pid',
                   timeout_job_after: 5.minutes,
                   env: Rails.env,
                   durable: true,
                   ack: true,
                   heartbeat: 2,
                   automatically_recover: true,
                   handler: Sneakers::Handlers::Maxretry,
                   daemonize: Rails.env.production?

Sneakers.logger = Rails.logger
Sneakers.logger.level = Logger::WARN
