Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :imports
      resources :customers, only: [:create]
      resources :countries, only: [:create]
      resources :cities, only: [:create]
      resources :provinces, only: [:create]
      resources :branch_offices, only: [:create]
      resources :unit_offices, only: [:create]
      resources :regionals, only: [:create]
      resources :areas, only: [:create]
      resources :customer_imports, only: [:index]
      resources :transaction_imports, only: [:index]
      resources :kas_imports, only: [:index]
    end
  end
end
